from aiogram import Bot, Dispatcher, executor, types
from config import BOT_TOKEN, user_id
from aiogram.utils import markdown
from aiogram.dispatcher import filters

# from sqlite import db_start, create_profile, edit_profile

bot = Bot(BOT_TOKEN, parse_mode='HTML')
dp = Dispatcher(bot)

async def send_to_admin(dp):
    await bot.send_message(chat_id=user_id, text='Бот запущен')

@dp.message_handler(commands=['start'])
async def handler_start(message):
    # bot.send_message(message.from_user_id, 'Привет❤️‍🩹')
    await message.answer('Привет❤️‍🩹')

@dp.message_handler(commands=['help'])
async def handler_help(message):
    # bot.send_message(message.from_user_id, '<i>Я - эхобот, я умею копировать в наш чат твои сообщения, фотографии и анимации 😍 <p>Со мной тебе не будет скучно!</p></i>')
    await message.answer('<i>Я - эхобот, я умею копировать в наш чат твои сообщения, фотографии и анимации 😍 \n<b>Со мной тебе не будет скучно!</b></i>')

@dp.message_handler(is_reply=True, commands='help')
@dp.message_handler(filters.IsReplyFilter(True), commands='help')
async def reply_filters_example(msg: types.Message):
    await msg.answer(msg.reply_to_message.from_user.id)

from aiogram.dispatcher import FSMContext

@dp.message_handler(commands='help')
async def help(msg: types.Message, state: FSMContext):
    'Присваиваем пользователю состояние'
    await state.set_state('example_state')
    await msg.answer('состояние установлено')

@dp.message_handler(
    content_types=types.ContentType.TEXT,
)
async def echo_msg(msg: types.Message):
    await msg.answer(markdown.quote_html(msg.text))
    await msg.answer(msg.text)


@dp.message_handler(
    content_types=types.ContentType.PHOTO,
)
async def echo_msg(msg: types.Message):
    await msg.reply_photo(msg.photo[-1].file_id, caption=msg.caption)


@dp.message_handler(
    content_types=types.ContentType.ANIMATION,
)
async def echo_msg(msg: types.Message):
    await msg.reply_animation(msg.animation[-1].file_id, caption=msg.caption)






if __name__ == '__main__':
    executor.start_polling(dp, on_startup=send_to_admin)


bot.polling(none_stop=True)