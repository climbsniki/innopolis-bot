from aiogram import types
from aiogram.dispatcher import filters
from aiogram import Bot, Dispatcher, executor, types
from config import BOT_TOKEN, user_id
from aiogram.utils import markdown

bot = Bot(BOT_TOKEN, parse_mode='HTML')
dp = Dispatcher(bot)

@dp.message_handler(is_reply=True, commands='help')
@dp.message_handler(filters.IsReplyFilter(True), commands='help')
async def reply_filters_example(msg: types.Message):
    await msg.answer(msg.reply_to_message.from_user.id)

from aiogram.dispatcher import FSMContext

@dp.message_handler(commands='help')
async def help(msg: types.Message, state: FSMContext):
    'Присваиваем пользователю состояние'
    await state.set_state('example_state')
    await msg.answer('состояние установлено')