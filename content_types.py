from aiogram import types, Dispatcher, executor, Bot
from aiogram.utils import markdown

from config import BOT_TOKEN
from dz2 import send_to_admin

bot = Bot(BOT_TOKEN, parse_mode='HTML')
dp = Dispatcher(bot)


@dp.message_handler(
    content_types=types.ContentType.TEXT,
)
async def echo_msg(msg: types.Message):
    await msg.answer(markdown.quote_html(msg.text))
    await msg.answer(msg.text)


@dp.message_handler(
    content_types=types.ContentType.PHOTO,
)
async def echo_msg(msg: types.Message):
    await msg.reply_photo(msg.photo[-1].file_id, caption=msg.caption)


@dp.message_handler(
    content_types=types.ContentType.ANIMATION,
)
async def echo_msg(msg: types.Message):
    await msg.reply_animation(msg.animation[-1].file_id, caption=msg.caption)


if __name__ == '__main__':
    executor.start_polling(dp, on_startup=send_to_admin)
