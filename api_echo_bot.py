import requests

from config import BOT_TOKEN

API_LINK = f'https://api.telegram.org/bot{BOT_TOKEN}'
data = {
    'offset': 0,
    'timeout': 60
}
while 1:
    updates = requests.get(API_LINK + '/getUpdates', data=data).json()
    print(updates)
    if not updates['ok'] or not updates['result']:
        continue
    for update in updates['result']:
        msg = update['message']
        if msg.get('text') is not None:
            data['offset'] = update['update_id'] + 1
            data_msg = {
                'chat_id': msg['chat']['id'],
                'text': msg['text']
            }
            sent_msg = requests.get(API_LINK + f'/sendMessage', data=data_msg)
