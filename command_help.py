from config import user_id, BOT_TOKEN
from aiogram import Bot, Dispatcher, executor

bot = Bot(BOT_TOKEN, parse_mode='HTML')
dp = Dispatcher(bot)

@dp.message_handler(commands=['help'])
async def handler_help(message):
    # bot.send_message(message.from_user_id, '<i>Я - эхобот, я умею копировать в наш чат твои сообщения, фотографии и анимации 😍 <p>Со мной тебе не будет скучно!</p></i>')
    await message.answer('<i>Я - эхобот, я умею копировать в наш чат твои сообщения, фотографии и анимации 😍 \n<b>Со мной тебе не будет скучно!</b></i>')
